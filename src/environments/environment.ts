// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: Environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAhX_G_44gqKSDKEhueYalMZxNkmTzxXgU',
    authDomain: 'seequence-app-staging.firebaseapp.com',
    databaseURL: 'https://seequence-app-staging.firebaseio.com',
    projectId: 'seequence-app-staging',
    storageBucket: 'seequence-app-staging.appspot.com',
    messagingSenderId: '478767069725',
    appId: '1:478767069725:web:64ade9029172ca49fae4f4'
  },
  defaultPhotoURL:
    // tslint:disable-next-line:max-line-length
    'https://firebasestorage.googleapis.com/v0/b/seequence-local.appspot.com/o/default%2Fprofile_placeholder_icon.svg?alt=media&token=e0ce2780-29d8-4c86-8d30-87658231738e',
  SegmentSourceKey: 'e9i81YEwHXQ9bO4Z8ikeTI3CclsAlyb4',
  crossOriginUrI: 'http://localhost:4200'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
