export const environment: Environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyAhX_G_44gqKSDKEhueYalMZxNkmTzxXgU',
    authDomain: 'seequence-app-staging.firebaseapp.com',
    databaseURL: 'https://seequence-app-staging.firebaseio.com',
    projectId: 'seequence-app-staging',
    storageBucket: 'seequence-app-staging.appspot.com',
    messagingSenderId: '478767069725',
    appId: '1:478767069725:web:64ade9029172ca49fae4f4'
  },
  defaultPhotoURL:
    // tslint:disable-next-line:max-line-length
    'https://firebasestorage.googleapis.com/v0/b/seequence-app.appspot.com/o/default%2Fprofile_placeholder_icon.svg?alt=media&token=15dd089f-c58d-41bf-862b-3225272fae67',
  SegmentSourceKey: 'e9i81YEwHXQ9bO4Z8ikeTI3CclsAlyb4',
  crossOriginUrI: 'https://seequence-app-staging.firebaseapp.com'
};
