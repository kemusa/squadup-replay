export const environment: Environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyB2KifWfwxxnd9nhnYknI7X8uyG3kyWJTM',
    authDomain: 'seequence-app.firebaseapp.com',
    databaseURL: 'https://seequence-app.firebaseio.com',
    projectId: 'seequence-app',
    storageBucket: 'seequence-app.appspot.com',
    messagingSenderId: '1056424428416',
    appId: '1:1056424428416:web:303d4862ed0678f398b765',
    measurementId: 'G-W5CPKB7KB9'
  },
  defaultPhotoURL:
    // tslint:disable-next-line:max-line-length
    'https://firebasestorage.googleapis.com/v0/b/seequence-d119a.appspot.com/o/default%2Fprofile_placeholder_icon.svg?alt=media&token=3f7bbb69-2b13-4d2c-94e6-a2c6cc2b30be',
  SegmentSourceKey: '8AUfLgmI3CZ4tcynrfw3ON13FOSTZm3e',
  crossOriginUrI: 'https://seequence.ai'
};
