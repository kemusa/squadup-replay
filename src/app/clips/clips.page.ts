import { Component, OnInit } from '@angular/core';
import { clips } from './clips.config';
import { NavController } from '@ionic/angular';
import { ClipsService } from './services/clips.service';

@Component({
  selector: 'app-clips',
  templateUrl: './clips.page.html',
  styleUrls: ['./clips.page.sass']
})
export class ClipsPage implements OnInit {
  clips = [...clips];
  topicIcon = '../../assets/images/topic-bjj-icon.svg';
  videoImageHeight;
  constructor(
    private navCtrl: NavController,
    private clipsService: ClipsService
  ) {}

  toClip(clip) {
    this.navCtrl.navigateForward(['clips', clip.id, clip.start, clip.end]);
  }

  ngOnInit() {}
}
