import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ApplicationRef
} from '@angular/core';
import {
  trigger,
  style,
  animate,
  animateChild,
  transition,
  query
} from '@angular/animations';
import { SplashService } from 'src/app/core/services/splash.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

@Component({
  selector: 'app-home-splash',
  templateUrl: './home-splash.component.html',
  styleUrls: ['./home-splash.component.sass'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('fadeOut', [
      transition(':leave', [
        query(':leave', animateChild(), { optional: true }),
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeSplashComponent implements OnInit {
  show = true;
  constructor(
    private splashService: SplashService,
    private cdr: ChangeDetectorRef,
    private appRef: ApplicationRef
  ) {}

  ngOnInit() {
    this.splashService.checkForUpdate().subscribe(result => {
      this.show = result;
      this.cdr.detectChanges();
    });
  }
}
