import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { DbService } from 'src/app/core/services/db.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.sass']
})
export class ContactFormComponent implements OnInit {
  input = {
    email: { title: 'Email', control: 'email', iconName: 'mail' },
    description: {
      title: 'Write your message here',
      control: 'description',
      iconName: 'list'
    }
  };
  form: FormGroup;
  updates = {};
  constructor(
    private formBuilder: FormBuilder,
    private db: DbService,
    private toastController: ToastController
  ) {}

  storeUpdates(key, event) {
    this.updates[key] = event.target.value;
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message:
        'Thank you for reaching out. A member of our team will be in touch soon!',
      duration: 5000,
      color: 'success'
    });
    toast.present();
  }

  async onSubmit() {
    await this.db.createDocument('inquiries', this.form.value);
    this.form.reset();
    this.presentToast();
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      description: ['', Validators.required]
    });
  }
}
