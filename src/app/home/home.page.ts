import { Component, OnInit } from '@angular/core';
import { screenWidths } from '../shared/consts';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.sass']
})
export class HomePage implements OnInit {
  hero_img_sm = '../../assets/images/home_hero_sm.png';
  hero_img_lg = '../../assets/images/home_hero_lg.png';
  logo = '../../assets/images/logo.svg';
  image_width = null;
  image_height = null;
  constructor() {}

  // get photo() {
  //   const notMobile = window.screen.width < screenWidths.lg;
  //   return notMobile ? this.hero_img_sm : this.hero_img_lg;
  // }

  // get scroll() {
  //   const canScroll = window.screen.width < screenWidths.lg;
  //   console.log(canScroll);
  //   return canScroll ? true : false;
  // }

  fitImage() {
    window.screen.width > screenWidths.md
      ? this.fitImageByHieght()
      : this.fitImageByWidth();
  }

  fitImageByHieght() {
    const height = window.innerHeight;
    this.image_height = `${height}px`;
    this.image_width = `${height * 0.65}px`;
  }

  fitImageByWidth() {
    const width = window.innerWidth;
    this.image_width = `${width}px`;
    this.image_height = `${width * 0.89}px`;
  }

  ngOnInit() {
    // if (window.screen.width > screenWidths.md) this.fitImage();
    window.screen.width > screenWidths.md
      ? this.fitImageByHieght()
      : this.fitImageByWidth();
  }
}
