import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputFieldLightComponent } from './components/input-field-light/input-field-light.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TextAreaLightComponent } from './components/text-area-light/text-area-light.component';
import { MenuHeaderComponent } from './components/menu-header/menu-header.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { TagFactoryComponent } from './components/tag-factory/tag-factory.component';
import { TagFactoryDarkComponent } from './components/tag-factory-dark/tag-factory-dark.component';

@NgModule({
  declarations: [
    InputFieldLightComponent,
    TextAreaLightComponent,
    MenuHeaderComponent,
    PageHeaderComponent,
    TagFactoryComponent,
    TagFactoryDarkComponent
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, IonicModule],
  exports: [
    InputFieldLightComponent,
    TextAreaLightComponent,
    MenuHeaderComponent,
    PageHeaderComponent,
    TagFactoryComponent,
    TagFactoryDarkComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {}
