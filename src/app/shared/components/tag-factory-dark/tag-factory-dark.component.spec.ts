import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TagFactoryDarkComponent } from './tag-factory-dark.component';

describe('TagFactoryDarkComponent', () => {
  let component: TagFactoryDarkComponent;
  let fixture: ComponentFixture<TagFactoryDarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagFactoryDarkComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TagFactoryDarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
