import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tag-factory',
  templateUrl: './tag-factory.component.html',
  styleUrls: ['./tag-factory.component.sass']
})
export class TagFactoryComponent implements OnInit {
  @Input() tag;
  @Input() size = 'medium';
  @Input() theme = 'light';
  constructor() {}

  styleTags() {
    let style;
    switch (this.size) {
      case 'small':
        style = { 'tag-small': true };
        break;
      case 'medium':
        style = { 'tag-medium': true };
        break;
      case 'large':
        style = { 'tag-large': true };
        break;
      default:
        style = { 'tag-small': true };
    }
    return style;
  }

  ngOnInit() {}
}
