import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  db: AngularFirestore;
  constructor(_db: AngularFirestore) {
    this.db = _db;
  }

  async createDocument(collectionName, data, id?) {
    const collection = await this.db.collection(collectionName);
    const success = id
      ? await collection.doc(id).set(data)
      : await collection.add(data);
    return success;
  }

  async updateDocument(collectionName, id, data) {
    const document = await this.db.collection(collectionName).doc(id);
    return document.update(data);
  }

  async getAllInCollection(collectionName, orderBy, direction?) {
    // if not order direction then default to descending
    const orderDir = direction ? direction : 'desc';
    const snapshot = await this.db
      .collection(collectionName, ref => ref.orderBy(orderBy, orderDir))
      .get()
      .toPromise();
    return snapshot.docs.map(doc => doc.data());
  }
}
